provider "aws" {
         access_key = "${var.access_key}"
         secret_key = "${var.secret_key}"
         region = "us-east-1"
}         

resource "aws_sns_topic" "user_updates" {
  name = "update-topic"
}

resource "aws_sns_topic_subscription" "notification_topic" {
  topic_arn = aws_sns_topic.user_updates.arn
  protocol  = "email"
  endpoint  = "${var.endpoint-email}"
}